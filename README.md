# ess-pynotify

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![pipeline](https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify/badges/master/pipeline.svg)](https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify/pipelines)
[![coverage](https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify/badges/master/coverage.svg)](https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify/pipelines)

`ess-pynotify` is a Python library to send notifications via the [ESS Notify Server](https://gitlab.esss.lu.se/ics-software/ess-notify-server).

It can be used both as a library and as a cli tool.

## Installation

Python >= 3.6 is required in every case.

### pip

```bash
pip3 install -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple ess-pynotify
```

Note that you can use [pipx](https://pipxproject.github.io/pipx/).

### shiv zipapp

The `send-ess-notification` script is available as a fully self-contained Python zipapp, built with [shiv](https://shiv.readthedocs.io/en/latest/index.html).
Only Python 3.6 interpreter is required. No other Python dependencies.

```bash
curl -O https://artifactory.esss.lu.se/artifactory/swi-pkg/ess-pynotify/<version>/send-ess-notification
chmod a+x send-ess-notification
```

Check the versions available here: <https://artifactory.esss.lu.se/artifactory/swi-pkg/ess-pynotify/>

## Usage

### cli tool

The main usage is to be used as a cli tool.

```bash
$ send-ess-notification --help
Usage: send-ess-notification [OPTIONS]

Options:
  --service-id UUID               Service id  [required]
  --title TEXT                    Notification title  [required]
  --subtitle TEXT                 Notification subtitle  [required]
  --url TEXT                      Notification url  [default: ]
  --server TEXT                   ESS Notification Server  [default:
                                  https://notify.esss.lu.se]

  --install-completion [bash|zsh|fish|powershell|pwsh]
                                  Install completion for the specified shell.
  --show-completion [bash|zsh|fish|powershell|pwsh]
                                  Show completion for the specified shell, to
                                  copy it or customize the installation.

  --help                          Show this message and exit.
```

### Python library

The send function can also be used directly from Python.

```python
import ess_pynotify


ess_pynotify.send(
    service_id="f48b4b51-e3c8-497b-a6da-2cb571d9d95e",
    title="My Message",
    subtitle="This is a test",
    url="",
)
```

## License

MIT
