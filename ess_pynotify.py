import uuid
import typer
import httpx

app = typer.Typer()


@app.command()
def send(
    service_id: uuid.UUID = typer.Option(..., help="Service id"),
    title: str = typer.Option(..., help="Notification title"),
    subtitle: str = typer.Option(..., help="Notification subtitle"),
    url: str = typer.Option("", help="Notification url"),
    server: str = typer.Option(
        "https://notify.esss.lu.se", help="ESS Notification Server"
    ),
):
    """Send a notification to the given service"""
    # If the function is imported directly to be used from Python,
    # the default values will be typer.models.OptionInfo objects.
    # Convert them manually (this is usually done by typer)
    if isinstance(url, typer.models.OptionInfo):
        url = url.default
    if isinstance(server, typer.models.OptionInfo):
        server = server.default
    data = {"title": title, "subtitle": subtitle, "url": url}
    try:
        response = httpx.post(
            f"{server}/api/v1/services/{service_id}/notifications", json=data
        )
        response.raise_for_status()
    except httpx.RequestError as exc:
        typer.secho(
            f"HTTP Exception for {exc.request.url} - {exc}",
            fg=typer.colors.RED,
            err=True,
        )
        raise typer.Exit(code=1)
    except httpx.HTTPStatusError as exc:
        typer.secho(f"{exc}", fg=typer.colors.BLUE, err=True)
        raise typer.Exit(code=1)
    else:
        typer.secho(f"Notification created: {response.json()}", fg=typer.colors.GREEN)
        return response.json()
