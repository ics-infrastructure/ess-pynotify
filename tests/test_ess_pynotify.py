import json
import httpx
import pytest
import uuid
import respx
from httpx import Response
from typer.testing import CliRunner

from ess_pynotify import app, send

BASE_URL = "https://notify.esss.lu.se/api/v1"
runner = CliRunner()


@pytest.mark.parametrize(
    "input, title, subtitle, url",
    [
        (
            ["--title", "First Message", "--subtitle", "This is a test"],
            "First Message",
            "This is a test",
            "",
        ),
        (
            [
                "--title",
                "Second Message",
                "--subtitle",
                "Another test",
                "--url",
                "https://google.com",
            ],
            "Second Message",
            "Another test",
            "https://google.com",
        ),
    ],
)
def test_app_correct_input(input, title, subtitle, url):
    service_id = uuid.uuid4()
    created_service = {"id": str(service_id)}
    with respx.mock:
        route = respx.post(
            f"https://notify.esss.lu.se/api/v1/services/{service_id}/notifications"
        )
        route.return_value = Response(201, json=created_service)
        result = runner.invoke(
            app,
            ["--service-id", str(service_id), *input],
        )
        assert result.exit_code == 0
        assert "Notification created" in result.stdout
        assert route.called
        assert route.call_count == 1
        data = {"title": title, "subtitle": subtitle, "url": url}
        assert route.calls.last.request._content.decode("utf-8") == json.dumps(data)


def test_app_invalid_service_id():
    service_id = 1234
    result = runner.invoke(
        app,
        ["--service-id", str(service_id), "--title", "Hello", "--subtitle", "test"],
    )
    assert result.exit_code == 2
    assert "1234 is not a valid UUID value" in result.stdout


@pytest.mark.parametrize("exc", [httpx.ConnectTimeout, httpx.ConnectError])
def test_app_request_error(exc):
    service_id = uuid.uuid4()
    with respx.mock:
        route = respx.post(
            f"https://notify.esss.lu.se/api/v1/services/{service_id}/notifications"
        )
        route.side_effect = exc
        result = runner.invoke(
            app,
            ["--service-id", str(service_id), "--title", "Hello", "--subtitle", "test"],
        )
        assert route.called
        assert result.exit_code == 1
        assert "HTTP Exception" in result.stdout


@pytest.mark.parametrize(
    "status_code, output",
    [
        (400, "Bad Request for url"),
        (404, "Not Found for url"),
        (422, "Unprocessable Entity for url"),
        (500, "Internal Server Error for url"),
    ],
)
def test_app_status_error(status_code, output):
    service_id = uuid.uuid4()
    url = f"https://notify.esss.lu.se/api/v1/services/{service_id}/notifications"
    with respx.mock:
        route = respx.post(url)
        route.side_effect = Response(status_code)
        result = runner.invoke(
            app,
            ["--service-id", str(service_id), "--title", "Hello", "--subtitle", "test"],
        )
        assert route.called
        assert result.exit_code == 1
        assert f"{output}: {url}" in result.stdout


@pytest.mark.parametrize(
    "input, title, subtitle, url",
    [
        ({"title": "Title", "subtitle": "text"}, "Title", "text", ""),
        (
            {"title": "Another", "subtitle": "text", "url": "https://google.com"},
            "Another",
            "text",
            "https://google.com",
        ),
    ],
)
def test_send_as_function(input, title, subtitle, url):
    service_id = uuid.uuid4()
    created_service = {"id": str(service_id)}
    with respx.mock:
        route = respx.post(
            f"https://notify.esss.lu.se/api/v1/services/{service_id}/notifications"
        )
        route.return_value = Response(201, json=created_service)
        result = send(str(service_id), **input)
        assert result == created_service
        assert route.called
        data = {"title": title, "subtitle": subtitle, "url": url}
        assert route.calls.last.request._content.decode("utf-8") == json.dumps(data)
