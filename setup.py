import setuptools

with open("README.md", "r") as f:
    long_description = f.read()


requirements = [
    "httpx",
    "typer",
    "colorama",
]
tests_requires = ["pytest", "pytest-cov", "respx"]

setuptools.setup(
    name="ess-pynotify",
    author="Benjamin Bertrand",
    author_email="benjamin.bertrand@ess.eu",
    description="Python package to send ESS notification",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify",
    license="MIT license",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    install_requires=requirements,
    py_modules=["ess_pynotify"],
    classifiers=[
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    extras_require={"tests": tests_requires},
    python_requires=">=3.6",
    entry_points={"console_scripts": ["send-ess-notification=ess_pynotify:app"]},
)
